﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : EventSubscriber {
	Vector3 initPos;
	Vector3 shakePos;
	float maxShakeDuration;
	float currentShakeDuration;
	float shakeIntensity;
	// Use this for initialization
	void Start () {
		maxShakeDuration = 0.25f;
		initPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected override void EnemyHitGroundEventResponse(object sender){
		StartCoroutine (ShakeCoroutine ());
	}

	IEnumerator ShakeCoroutine(){
		currentShakeDuration = 0.0f;
		while (currentShakeDuration <= maxShakeDuration) {
			currentShakeDuration += Time.deltaTime;
			float shakeFactor = MyUtilities.MapValueFloat (
				GameController.instance.currentGameDifficulty,
				0.0f,
				1.0f,
				0.5f,
				0.0f
			);
			Vector3 randomShake = new Vector3 (
				Random.Range(-shakeFactor,shakeFactor),
				Random.Range(-shakeFactor,shakeFactor),
				Random.Range(-shakeFactor,shakeFactor)
			);
			shakePos = initPos + randomShake;
			transform.position = shakePos;
			yield return null;
		}
		transform.position = initPos;
	}
}
