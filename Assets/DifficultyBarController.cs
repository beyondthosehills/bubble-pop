﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyBarController : MonoBehaviour {
	public Color easyColor;
	public Color difficultColor;
	public Color mixedColor;
	public Image difficultyBarFill;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		mixedColor.r = MyUtilities.MapValueFloat (
			GameController.instance.currentGameDifficulty,
			0.0f,
			1.0f,
			easyColor.r,
			difficultColor.r
		);
		mixedColor.g = MyUtilities.MapValueFloat (
			GameController.instance.currentGameDifficulty,
			0.0f,
			1.0f,
			easyColor.g,
			difficultColor.g
		);
		mixedColor.b = MyUtilities.MapValueFloat (
			GameController.instance.currentGameDifficulty,
			0.0f,
			1.0f,
			easyColor.b,
			difficultColor.b
		);
		mixedColor.a = MyUtilities.MapValueFloat (
			GameController.instance.currentGameDifficulty,
			0.0f,
			1.0f,
			easyColor.a,
			difficultColor.a
		);
		difficultyBarFill.color = mixedColor;
		float fillAmount = GameController.instance.currentGameDifficulty;
		difficultyBarFill.fillAmount = fillAmount;
	}
}
