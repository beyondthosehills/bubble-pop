﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : EventSubscriber {
	public GameState gameState; 
	public int currentScore;
	public int previousScore;
	public int highScore;
	public float currentGameTime;
	public float currentGameDifficulty;
	public float maxGameDifficulty = 1.0f;
	public float playerEnergy = 100.0f;
	public static GameController instance = null;
	public float maxGameDifficultyTime = 30.0f;


	void Awake(){
		if (instance == null) {
			instance = this;
		}
		else if(instance != this){
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		gameState = GameState.playing;
		MyEventManager.instance.RaiseGameDeactivatedEvent (this);
	}
	
	// Update is called once per frame
	void Update () {
		if (gameState == GameState.playing) {
			currentGameTime += Time.deltaTime;
			if (currentGameTime <= maxGameDifficultyTime) {
				currentGameDifficulty = MyUtilities.MapValueFloat (currentGameTime, 0.0f, maxGameDifficultyTime, 0.0f, 1.0f);
			} else {
				currentGameDifficulty = maxGameDifficulty;
			}
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			if (gameState == GameState.playing) {
				gameState = GameState.paused;
				MyEventManager.instance.RaiseGamePausedEvent (this);
			}
			else if (gameState == GameState.paused) {
				gameState = GameState.playing;
				MyEventManager.instance.RaiseGameResumedEvent (this);
			}
		}
	}

	protected override void GameStartedEventResponse(object sender){	
		gameState = GameState.playing;
		playerEnergy = 100.0f;
		currentScore = 0;
		currentGameTime = 0.0f;
	}

	protected override void GamePausedEventResponse(object sender){	
		gameState = GameState.paused;
	}

	protected override void GameResumedEventResponse(object sender){	
		gameState = GameState.playing;
	}

	protected override void GameOverEventResponse(object sender){
		gameState = GameState.gameOver;	
		previousScore = currentScore;
		if (previousScore > highScore) {
			highScore = previousScore;
		}
		currentScore = 0;
		currentGameTime = 0.0f;
	}

	protected override void GameDeactivatedEventResponse(object sender){
		gameState = GameState.deactivated;
	}

	protected override void ApplicationQuitEventResponse(object sender){	
		Application.Quit ();
	}

	protected override void EnemyKilledEventResponse(object sender){	
		currentScore += 1;
	}

	protected override void EnemyHitGroundEventResponse(object sender){
		Debug.Log("Enemy Hit Ground " + playerEnergy.ToString());
		playerEnergy -= 10.0f;
		if (playerEnergy <= 0.0f) {
			MyEventManager.instance.RaiseGameOverEvent (this);
		}
	}
}

public enum GameState{
	playing,
	paused,
	gameOver,
	deactivated,
	credits
}
