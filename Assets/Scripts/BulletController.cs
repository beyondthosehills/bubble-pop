﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : EventSubscriber {
	public GameObject shootSoundPrefab;
	Vector3 pos;
	Vector3 velocity;
	public float speed;
	Vector3 initPosition;
	[SerializeField]
	float destroyDistance;
	public bool active;
	bool paused;
	// Use this for initialization
	void Awake () {
		initPosition = this.transform.position;
		active = false;
		paused = false;
	}

	void Start(){
		Instantiate (shootSoundPrefab, transform.position, Quaternion.identity);
	}
	// Update is called once per frame
	void Update () {
		if (active && !paused) {
			pos = transform.position;
			pos = pos + velocity * Time.deltaTime;
			transform.position = pos;
			if (Vector3.Distance (this.initPosition, this.transform.position) > destroyDistance) {

				Destroy (this.gameObject);
			}
		}

	}

	public void SetDirection(Vector3 direction){
		velocity = direction.normalized * speed;
		active = true;
	}

	protected override void GamePausedEventResponse(object sender){	
		paused = true;
	}

	protected override void GameResumedEventResponse(object sender){	
		paused = false;
	}
}
