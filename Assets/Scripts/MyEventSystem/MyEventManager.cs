﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;


public class MyEventManager : MonoBehaviour {

	public static MyEventManager instance = null;

	void Awake(){
		if (instance == null) {
			instance = this;
		}
		else if(instance != this){
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}


	public delegate void GameStartedEvent(object sender);
	public static event GameStartedEvent GameStartedEvt;

	public delegate void GamePausedEvent(object sender);
	public static event GamePausedEvent GamePausedEvt;

	public delegate void GameResumedEvent(object sender);
	public static event GameResumedEvent GameResumedEvt;

	public delegate void GameOverEvent(object sender);
	public static event GameOverEvent GameOverEvt;

	public delegate void GameDeactivatedEvent(object sender);
	public static event GameDeactivatedEvent GameDeactivatedEvt;

	public delegate void ApplicationQuitEvent(object sender);
	public static event ApplicationQuitEvent ApplicationQuitEvt;

	public delegate void EnemyKilledEvent(object sender);
	public static event EnemyKilledEvent EnemyKilledEvt;

	public delegate void EnemyHitGroundEvent(object sender);
	public static event EnemyHitGroundEvent EnemyHitGroundEvt;

	public void RaiseGameStartedEvent(object sender){
		if (GameStartedEvt != null) {
			GameStartedEvt (sender);
		}
	}

	public void RaiseGamePausedEvent(object sender){
		if (GamePausedEvt != null) {
			GamePausedEvt (sender);
		}
	}

	public void RaiseGameResumedEvent(object sender){
		if (GameResumedEvt != null) {
			GameResumedEvt (sender);
		}
	}

	public void RaiseGameOverEvent(object sender){
		if (GameOverEvt != null) {
			GameOverEvt (sender);
		}
	}

	public void RaiseGameDeactivatedEvent(object sender){
		if (GameDeactivatedEvt != null) {
			GameDeactivatedEvt (sender);
		}
	}

	public void RaiseApplicationQuitEvent(object sender){
		if (ApplicationQuitEvt != null) {
			ApplicationQuitEvt (sender);
		}
	}

	public void RaiseEnemyKilledEvent(object sender){
		if (EnemyKilledEvt != null) {
			EnemyKilledEvt (sender);
		}
	}

	public void RaiseEnemyHitGroundEvent(object sender){
		if (EnemyHitGroundEvt != null) {
			EnemyHitGroundEvt (sender);
		}
	}
		
}

	