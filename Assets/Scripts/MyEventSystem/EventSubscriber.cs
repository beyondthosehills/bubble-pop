﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public abstract class EventSubscriber : MonoBehaviour {


	void OnEnable(){
		SubscribeToAllEvents ();
	}

	void OnDisable(){
		UnsubscribeFromAllEvents ();
	}

	protected void SubscribeToAllEvents(){
		MyEventManager.GameStartedEvt += GameStartedEventResponse;
		MyEventManager.GamePausedEvt += GamePausedEventResponse;
		MyEventManager.GameResumedEvt += GameResumedEventResponse;
		MyEventManager.GameOverEvt += GameOverEventResponse;
		MyEventManager.GameDeactivatedEvt += GameDeactivatedEventResponse;
		MyEventManager.ApplicationQuitEvt += ApplicationQuitEventResponse;
		MyEventManager.EnemyKilledEvt += EnemyKilledEventResponse;
		MyEventManager.EnemyHitGroundEvt += EnemyHitGroundEventResponse;
	}

	protected void UnsubscribeFromAllEvents(){
		MyEventManager.GameStartedEvt -= GameStartedEventResponse;
		MyEventManager.GamePausedEvt -= GamePausedEventResponse;
		MyEventManager.GameResumedEvt -= GameResumedEventResponse;
		MyEventManager.GameOverEvt -= GameOverEventResponse;
		MyEventManager.GameDeactivatedEvt -= GameDeactivatedEventResponse;
		MyEventManager.ApplicationQuitEvt -= ApplicationQuitEventResponse;
		MyEventManager.EnemyKilledEvt -= EnemyKilledEventResponse;
		MyEventManager.EnemyHitGroundEvt -= EnemyHitGroundEventResponse;
	}

	protected virtual void GameStartedEventResponse(object sender){	}
	protected virtual void GamePausedEventResponse(object sender){	}
	protected virtual void GameResumedEventResponse(object sender){	}
	protected virtual void GameOverEventResponse(object sender){	}
	protected virtual void GameDeactivatedEventResponse(object sender){	}
	protected virtual void ApplicationQuitEventResponse(object sender){	}
	protected virtual void EnemyKilledEventResponse(object sender){	}
	protected virtual void EnemyHitGroundEventResponse(object sender){	}


}
