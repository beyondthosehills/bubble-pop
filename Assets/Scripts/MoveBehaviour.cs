﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvoidTheCube
{

    public class MoveBehaviour : MonoBehaviour 
    {
        [SerializeField]
        float speed;
        [SerializeField]
        Vector3 direction;
        [SerializeField]
        float destroyDistance;

        Vector3 initPosition;

    	// Use this for initialization
    	void Start () 
        {
            initPosition = this.transform.position;
    	}
    	
    	// Update is called once per frame
    	void Update () 
        {
            this.transform.position += direction.normalized * (speed * Time.deltaTime);

            if (Vector3.Distance (this.initPosition, this.transform.position) > destroyDistance)
            {
                Destroy (this.gameObject);
            }
    	}
    }
}