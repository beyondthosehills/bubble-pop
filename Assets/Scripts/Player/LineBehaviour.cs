﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBehaviour : EventSubscriber {
	LineRenderer lineRenderer;
	public GameObject target;
	public GameObject spawnPositionHolder;
	// Use this for initialization
	void Start () {
		lineRenderer = GetComponent<LineRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 startPos = new Vector3 (spawnPositionHolder.transform.position.x, spawnPositionHolder.transform.position.y, spawnPositionHolder.transform.position.z);
		Vector3 endPos = target.transform.position;
		lineRenderer.SetPosition (0, startPos);
		lineRenderer.SetPosition (1, endPos);
	}

	protected override void GameStartedEventResponse(object sender){	

	}

	protected override void GamePausedEventResponse(object sender){	
		Debug.Log("paused");
		lineRenderer.enabled = false;
	}

	protected override void GameResumedEventResponse(object sender){	
		Debug.Log("resumed");
		lineRenderer.enabled = true;
	}

	protected override void GameOverEventResponse(object sender){	

	}
}
