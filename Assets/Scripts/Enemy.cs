﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvoidTheCube
{
    public class Enemy : EventSubscriber {

		Color myColor;
		public GameObject dieByBulletSoundPrefab;
		public GameObject dieByDistanceSoundPrefab;
		public GameObject enemyExplosionPrefab;

		GameObject player;
		[SerializeField]
		float speed;
		[SerializeField]
		Vector3 direction;
		[SerializeField]
		float destroyDistance;

		Vector3 initPosition;

		bool paused;
    	// Use this for initialization
    	void Start () {
			speed = MyUtilities.MapValueFloat(
				GameController.instance.currentGameDifficulty,
				0.0f,	// minimum difficulty
				1.0f,	// maximum difficulty
				1.5f,	// minimum speed
				5.0f	// maximum speed
			);
			myColor = new Color (
				Random.Range(0.25f,1.0f),
				Random.Range(0.25f,1.0f),
				Random.Range(0.25f,1.0f)
			);
			GetComponent<SpriteRenderer> ().color = myColor;
			float sc = Random.Range (0.5f,0.75f);
			Vector3 scale = new Vector3 (sc, sc, sc);
			transform.localScale = scale;
			initPosition = this.transform.position;
			paused = false;
		}


    	
    	// Update is called once per frame
    	void Update () {
			if (!paused) {
				this.transform.position += direction.normalized * (speed * Time.deltaTime);
			}

    	}

		void OnCollisionEnter2D(Collision2D col)
		{
			//Debug.Log ("collision");
			if (col.gameObject.tag == "bullet") {
				MyEventManager.instance.RaiseEnemyKilledEvent (this);
				GameObject exp = Instantiate (enemyExplosionPrefab, transform.position, Quaternion.identity);
				exp.transform.localScale = this.transform.localScale;
				exp.GetComponent<SpriteRenderer> ().color = GetComponent<SpriteRenderer> ().color;
				Instantiate (dieByBulletSoundPrefab, transform.position, Quaternion.identity);
				Destroy (col.gameObject);
				Destroy (this.gameObject);
			}

			if (col.gameObject.tag == "ground") {
				MyEventManager.instance.RaiseEnemyHitGroundEvent (this);
				GameObject exp = Instantiate (enemyExplosionPrefab, transform.position, Quaternion.identity);
				exp.transform.localScale = this.transform.localScale;
				exp.GetComponent<SpriteRenderer> ().color = GetComponent<SpriteRenderer> ().color;
				Instantiate (dieByDistanceSoundPrefab, transform.position, Quaternion.identity);
				Destroy (this.gameObject);
			}
		}

		protected override void GameStartedEventResponse(object sender){	

		}

		protected override void GamePausedEventResponse(object sender){	
			paused = true;
		}

		protected override void GameResumedEventResponse(object sender){	
			paused = false;
		}

		protected override void GameOverEventResponse(object sender){	
			Destroy (this.gameObject);
		}
    }
}