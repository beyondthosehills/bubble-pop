﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyUtilities {

	/// <summary>
	/// Maps a float value from one range to another.
	/// </summary>
	/// <returns>The new float value.</returns>
	/// <param name="value">Value.</param>
	/// <param name="min1">Min1.</param>
	/// <param name="max1">Max1.</param>
	/// <param name="min2">Min2.</param>
	/// <param name="max2">Max2.</param>
	public static float MapValueFloat(float value, float min1, float max1, float min2, float max2){
		return (value - min1) / (max1 - min1) * (max2 - min2) + min2;
	}

	/// <summary>
	/// Maps a double value from one range to another.
	/// </summary>
	/// <returns>The new double value.</returns>
	/// <param name="value">Value.</param>
	/// <param name="min1">Min1.</param>
	/// <param name="max1">Max1.</param>
	/// <param name="min2">Min2.</param>
	/// <param name="max2">Max2.</param>
	public static double MapValueDouble(double value, double min1, double max1, double min2, double max2){
		return (value - min1) / (max1 - min1) * (max2 - min2) + min2;
	}

	/// <summary>
	/// Blends two RGBA colors.
	/// </summary>
	/// <returns>The blend RGB.</returns>
	/// <param name="mixValue">Mix value.</param>
	/// <param name="color1">Color1.</param>
	/// <param name="color2">Color2.</param>
	public static Color ColorBlendRGBA(float mixValue, Color color1, Color color2){
		float r1 = color1.r;
		float g1 = color1.g;
		float b1 = color1.b;
		float a1 = color1.a;

		float r2 = color2.r;
		float g2 = color2.g;
		float b2 = color2.b;
		float a2 = color2.a;

		float rMix = MapValueFloat (mixValue, 0.0f, 1.0f, r1, r2);
		float gMix = MapValueFloat (mixValue, 0.0f, 1.0f, g1, g2);
		float bMix = MapValueFloat (mixValue, 0.0f, 1.0f, b1, b2);
		float aMix = MapValueFloat (mixValue, 0.0f, 1.0f, a1, a2);

		Color outColor = new Color (rMix, gMix, bMix, aMix);
		return outColor;
	}



}
