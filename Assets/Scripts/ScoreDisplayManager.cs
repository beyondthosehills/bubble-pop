﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplayManager : MonoBehaviour {
	public Text myText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		string txt = "Current Score: ";
		txt += GameController.instance.currentScore;
		txt += "\nPrevious Score: ";
		txt += GameController.instance.previousScore;
		txt += "\nHigh Score: ";
		txt += GameController.instance.highScore;
		txt += "\nEnergy: ";
		txt += GameController.instance.playerEnergy;
		myText.text = txt;
	}
}
