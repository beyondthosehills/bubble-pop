﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButtonController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void BackToMenuScreen(){
		MyEventManager.instance.RaiseGameDeactivatedEvent (this);
	}
}
