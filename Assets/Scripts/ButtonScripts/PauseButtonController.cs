﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButtonController : EventSubscriber {
	public Button button;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected virtual void GameStartedEventResponse(object sender){	
		button.interactable = true;
	}

	protected virtual void GameFinishedEventResponse(object sender){	
		button.interactable = false;
	}

	public void GamePauseResume(){
		if (GameController.instance.gameState == GameState.paused) {
			MyEventManager.instance.RaiseGameResumedEvent (this);
		}
		else if(GameController.instance.gameState == GameState.playing){
			MyEventManager.instance.RaiseGamePausedEvent (this);
		}
	}
}
