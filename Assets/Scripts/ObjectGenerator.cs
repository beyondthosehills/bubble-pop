﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvoidTheCube
{
	public class ObjectGenerator : EventSubscriber {
		bool active;
        [SerializeField]
        GameObject[] objects;
        public float nextSpawnDuration = 5f;
        [SerializeField]
        float spawnRange = 5f;

        float timer = 0f;

    	// Use this for initialization
    	void Awake () 
        {
			active = false;
    	}
    	
    	// Update is called once per frame
    	void Update () 
        {
			nextSpawnDuration = MyUtilities.MapValueFloat (
				GameController.instance.currentGameDifficulty,
				0.0f,	// minimum difficulty
				1.0f,	// maximum difficulty
				1.0f,	// maximum spawn time
				0.15f	// minimum spawn time
			);
			if (active) {
				timer += Time.deltaTime;

				if (timer > nextSpawnDuration) {
					RandomSpawn ();
					timer = 0f;
				}
			}
    	}

		public void Activate(){
			active = true;
		}

		public void Deactivate(){
			active = false;
		}

        void RandomSpawn ()
        {
            //Debug.Log ("Spawn");

            if (objects == null || objects.Length == 0)
                return;

            int randomIndex = Random.Range (0, objects.Length);

            GameObject objectToSpawn = objects[randomIndex];
            Vector3 spawnPosition = this.transform.position + (Vector3.right * Random.Range (spawnRange / 2f * -1f, spawnRange / 2f));

            Instantiate<GameObject> (objectToSpawn, spawnPosition, Quaternion.identity);
        }

        void OnDrawGizmos ()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube (this.transform.position, new Vector3(spawnRange,1f,1f));
        }

		protected override void GameStartedEventResponse(object sender){	
			Activate ();
		}

		protected override void GamePausedEventResponse(object sender){	
			Deactivate ();
		}

		protected override void GameResumedEventResponse(object sender){	
			Activate ();
		}

		protected override void GameOverEventResponse(object sender){
			Deactivate ();
		}

		protected override void GameDeactivatedEventResponse(object sender){
			Deactivate ();
		}
    }
}
