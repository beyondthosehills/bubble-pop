﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFXPrefabController : MonoBehaviour {
	public AudioClip audioClip;
	public AudioSource audioPlayer;
	// Use this for initialization
	void Start () {
		audioPlayer.pitch = Random.Range (0.85f, 1.15f);
		audioPlayer.volume = Random.Range (0.25f, 0.5f);
		audioPlayer.PlayOneShot (audioClip);
	}

	// Update is called once per frame
	void Update () {
		if (!audioPlayer.isPlaying) {
			Destroy (this.gameObject);
		}
	}
}
