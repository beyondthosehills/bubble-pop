﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AvoidTheCube
{
	public class Player : EventSubscriber 
    {
		bool active;
		public GameObject bulletPrefab;
		public GameObject spawnPositionHolder;
        [SerializeField]
        int initLife = 3;
        [SerializeField]
        float speed = 1f;

		public GameObject target;
		Vector3 bulletSpawnPosition;
		Vector3 targetPosition;

        int life;

    	// Use this for initialization
    	void Start () 
        {
            life = initLife;
    	}
    	
    	// Update is called once per frame
    	void Update () {
			if (active) {
				if (Input.GetMouseButtonDown (0)) {
					Vector3 myPos = transform.position;
					Quaternion quat = bulletPrefab.transform.rotation;
					Vector3 bulletSpawnPos = new Vector3 (spawnPositionHolder.transform.position.x, spawnPositionHolder.transform.position.y, spawnPositionHolder.transform.position.z);
					Vector3 shootDirection = target.transform.position - transform.position;
					GameObject spawnedBullet = Instantiate (bulletPrefab, bulletSpawnPos, Quaternion.identity);
					spawnedBullet.GetComponent<BulletController> ().SetDirection (shootDirection);
				}
			}
    	}

		protected override void GameStartedEventResponse(object sender){
			active = true;
		}
		protected override void GamePausedEventResponse(object sender){
			active = false;
		}
		protected override void GameResumedEventResponse(object sender){
			active = true;
		}
		protected override void GameOverEventResponse(object sender){
			active = false;
		}
		protected override void GameDeactivatedEventResponse(object sender){
			active = false;
		}
    }
}