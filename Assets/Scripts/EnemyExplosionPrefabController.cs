﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyExplosionPrefabController : MonoBehaviour {
	Color myColor;
	Sprite mySprite;
	float fadeTime;
	float initTime;
	float currentDuration;
	// Use this for initialization
	void Start () {
		mySprite = GetComponent<SpriteRenderer> ().sprite;
		myColor = GetComponent<SpriteRenderer> ().color;
		fadeTime = 0.25f;
		initTime = Time.time;

		StartCoroutine (FadeCoroutine ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator FadeCoroutine(){
		while (currentDuration < fadeTime) {
			currentDuration += Time.deltaTime;
			float alpha = map (currentDuration, 0.0f, fadeTime, 1.0f, 0.0f);
			myColor.a = alpha;
			GetComponent<SpriteRenderer> ().color = myColor;
			yield return null;
		}
		Destroy (this.gameObject);
	}

	float map(float s, float a1, float a2, float b1, float b2)
	{
		return b1 + (s-a1)*(b2-b1)/(a2-a1);
	}
}
