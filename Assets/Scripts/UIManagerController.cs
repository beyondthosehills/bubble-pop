﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerController : EventSubscriber {
	public GameObject pauseScreen;
	public GameObject gameOverScreen;
	public GameObject introScreen;
	public GameObject inGameUI;
	public Button pauseButton;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	protected override void GamePausedEventResponse(object sender){	
		pauseScreen.SetActive( true);
	}

	protected override void GameResumedEventResponse(object sender){	
		pauseScreen.SetActive( false);
	}

	protected override void GameOverEventResponse(object sender){
		pauseButton.interactable = false;
		gameOverScreen.SetActive (true);
		inGameUI.SetActive (false);
	}

	protected override void GameStartedEventResponse(object sender){
		pauseButton.interactable = true;
		gameOverScreen.SetActive (false);
		pauseScreen.SetActive( false);
		introScreen.SetActive( false);
		inGameUI.SetActive (true);
	}

	protected override void GameDeactivatedEventResponse(object sender){
		gameOverScreen.SetActive (false);
		pauseScreen.SetActive( false);
		introScreen.SetActive( true);
		inGameUI.SetActive (false);
	}
}
