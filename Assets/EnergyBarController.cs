﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBarController : MonoBehaviour {
	public Color fullColor;
	public Color emptyColor;
	public Color mixedColor;
	public Image energyBarFill;
	public float energy;
	// Use this for initialization
	void Start () {

	}
		
	void Update () {
		energy = GameController.instance.playerEnergy;
		mixedColor.r = MyUtilities.MapValueFloat (
			GameController.instance.playerEnergy,
			100f,
			0f,
			fullColor.r,
			emptyColor.r
		);
		mixedColor.g = MyUtilities.MapValueFloat (
			GameController.instance.playerEnergy,
			100f,
			0f,
			fullColor.g,
			emptyColor.g
		);
		mixedColor.b = MyUtilities.MapValueFloat (
			GameController.instance.playerEnergy,
			100f,
			0f,
			fullColor.b,
			emptyColor.b
		);
		mixedColor.a = MyUtilities.MapValueFloat (
			GameController.instance.playerEnergy,
			100f,
			0f,
			fullColor.a,
			emptyColor.a
		);
		energyBarFill.color = mixedColor;
		float fillAmount = MyUtilities.MapValueFloat (
			GameController.instance.playerEnergy,
			100.0f,
			0.0f,
			1.0f,
			0.0f
		);
		energyBarFill.fillAmount = fillAmount;
	}
}
